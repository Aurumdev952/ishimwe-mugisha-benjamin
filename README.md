# simp-calc

this is a restful api for a simple calculator

## getting started

to install it via npm 

run

```

npm i simp-calc-by-ben

```

to download it

```

git clone https://gitlab.com/Aurumdev952/ishimwe-mugisha-benjamin.git

```


## usage

run ``` simp-calc ``` to start the server, it should run on url **http://localhost:8080/**

 ## addition
 
make a GET request on **http://localhost:8080/api/add** and provide a JSON object in the body containing the numbers you want to add

example

 ```

{
    "numbers": [1, 2, 3, 4, 5]
}

```
 ## multiplication

make a GET request on **http://localhost:8080/api/multiply** and provide a JSON object in the body containing the numbers you want to multiply

example

 ```

{
    "numbers": [1, 2, 3, 4, 5]
}

```