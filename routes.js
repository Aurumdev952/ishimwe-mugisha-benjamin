const express = require('express');
const router = express.Router();
const {
    multiplyList,
    addList
} = require('./functions/functions')
router.get('/add', async (req, res) => {
    try {
        const numbers = req.body.numbers
        const answer = addList(numbers)
        if (answer != null) {
            res.status(200).json({
                answer: answer
            })
        } else {
            res.status(404).json({
                status: 'error',
                message: "input correct numbers"
            })
        }
    } catch (err) {
        console.log(err.message);
        res.status(500).json({
            status: 'error',
            message: err.message
        })
    }
})
router.get('/multiply', async (req, res) => {
    try {
        const numbers = req.body.numbers
        const answer = multiplyList(numbers)
        if (answer != null) {
            res.status(200).json({
                answer: answer
            })
        } else {
            res.status(404).json({
                status: 'error',
                message: "input correct numbers"
            })
        }
    } catch (err) {
        console.log(err.message);
        res.status(500).json({
            status: 'error',
            message: err.message
        })
    }
})



module.exports = router