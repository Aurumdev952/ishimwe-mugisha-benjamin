// this function is to chech if the inputed array is correct
function checkNumbers(numbers) {
    if (numbers?.length > 0) {
        for (let n of numbers) {
            if (typeof n !== 'number') {
                return false;
            }
        }
    } else {
        return false;
    }
    return true;
}

function addList(numbers) {
    if (checkNumbers(numbers)) {
        let sum = 0;
        numbers.forEach(num => {
            sum += num
        })
        return sum
    } else {
        return null;
    }
    
}
function multiplyList(numbers) {
    if (checkNumbers(numbers)) {
        let product = 1;
        numbers.forEach(num => {
            product *= num
        })
        return product
    } else {
        return null;
    }
    
}




module.exports = {
    multiplyList,
    addList
}
